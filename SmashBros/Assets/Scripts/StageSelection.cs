﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageSelection : MonoBehaviour
{
    public void toStage1()
    {
        SceneManager.LoadScene("Mapa1");
    }

    public void toStage2()
    {
        SceneManager.LoadScene("Mapa2");
    }
}
