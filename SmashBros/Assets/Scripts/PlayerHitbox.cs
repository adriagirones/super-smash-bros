﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitbox : MonoBehaviour
{
    public int nHitbox;

    public delegate void damagePlayer(float n);
    public event damagePlayer damagePlayerEvent;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "EnemyHurtbox":
                GameObject parent = collision.gameObject.transform.parent.gameObject;
                
                switch (this.transform.parent.gameObject.GetComponent<Player>().getComboStatus())
                {
                    case 1:
                        Attack(parent, 5f, 20f, 0.025f * nHitbox);
                        break;
                    case 2:
                        Attack(parent, 10f, 20f, 0.025f * nHitbox);
                        break;
                    case 3:
                        Attack(parent, 30f, 35f, 0.05f * nHitbox);
                        break;
                    case 4:
                        Attack(parent, 50f, 75f, 0.10f * nHitbox);
                        break;
                }
                break;
        }
    }

    //DEPENENT DE L'ATAC, TINDRÀ UN KNOCKBACK DIFERENT
    void Attack(GameObject parent, float _x, float _y, float _damage)
    {
        float knockBack = parent.gameObject.GetComponent<Enemy>().vida;
        parent.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        if (this.transform.parent.gameObject.GetComponent<Player>().transform.position.x >= parent.gameObject.GetComponent<Transform>().position.x)
        {
            parent.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-_x * (knockBack), _y * (knockBack)));
        }
        else
        {
            parent.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(_x * (knockBack), _y * (knockBack)));
        }
        if (damagePlayerEvent != null)
        {
            damagePlayerEvent.Invoke(_damage);
        }
    }


}
