﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float velocity;
    public float vida = 1f;

    GameObject hitbox1;
    GameObject hitbox2;
    GameObject colliderBox;
    GameObject shield;

    public GameObject fireball;
    public GameObject player;

    public GameObject particles;
    public GameObject particles2;

    Animator anim;
    Rigidbody2D rb;

    int comboStatus;
    bool isGrounded;
    bool doubleJump;

    float dirX = 0f;

    bool facingLeft = true;
    Vector3 localScale;

    bool flagMove = true;
    bool flagShield = false;
    bool iframes = false;
    bool cooldownBall = false;

    public delegate void updateEnemyUI(int n);
    public event updateEnemyUI updateEnemyUIEvent;

    public Pool poolManager;

    int stock;

    GameObject soundmanager;

    void Start()
    {
        colliderBox = transform.GetChild(1).gameObject;
        shield = transform.GetChild(2).gameObject;
        hitbox1 = transform.GetChild(3).gameObject;
        hitbox2 = transform.GetChild(4).gameObject;

        colliderBox.GetComponent<PlayerTrigger>().activateColliderEvent += activateCollider;
        hitbox1.SetActive(false);
        hitbox2.SetActive(false);
        shield.SetActive(false);

        anim = GetComponent<Animator>();
        rb = this.GetComponent<Rigidbody2D>();
        localScale = transform.localScale;

        GameObject playerHitbox1 = player.transform.GetChild(3).gameObject;
        GameObject playerHitbox2 = player.transform.GetChild(4).gameObject;
        playerHitbox1.GetComponent<PlayerHitbox>().damagePlayerEvent += lessLive;
        playerHitbox2.GetComponent<PlayerHitbox>().damagePlayerEvent += lessLive;

        stock = 3;

        soundmanager = GameObject.Find("SoundManager");
    }

    void Update()
    {
        //MOVIMENT
        if (Input.GetKey("j") && flagMove)
        {
            dirX = -velocity;
            rb.velocity = new Vector2(-velocity, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey("l") && flagMove)
        {
            dirX = velocity;
            rb.velocity = new Vector2(velocity, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            dirX = 0;
        }
        if (!isGrounded)
        {
            rb.velocity = new Vector2(dirX, rb.velocity.y);
        }

        //DOBLE SALT
        if (!isGrounded && Input.GetKeyDown("i") && !doubleJump && flagMove)
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXMarioJump2();
            //RESETEJA LA VELOCITAT PER FER UN DOBLE SALT AMB LA MATEIXA FORÇA QUE EL PRIMER
            rb.velocity = new Vector2(0, 0);
            rb.AddForce(new Vector2(0, 350f));
            doubleJump = true;
        }
        //SALT
        if (isGrounded && Input.GetKeyDown("i") && flagMove)
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXMarioJump();
            rb.AddForce(new Vector2(0, 350f));
            isGrounded = false;
        }

        //COMBOS
        if (Input.GetKeyDown("o"))
        {
            flagMove = false;
            if(isGrounded)
                StartCoroutine(combo("o", 0.5f));
        }
        else if (Input.GetKeyDown("p"))
        {
            if (isGrounded)
                StartCoroutine(combo("p", 0.8f));
        }

        //FIREBALL
        if (Input.GetKeyDown("u") && flagMove && !cooldownBall)
        {
            GameObject b = poolManager.GetObjFromPool();
            b.GetComponent<FireBall>().enableFireBall();
            b.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
            b.GetComponent<FireBall>().damagePlayerEvent += player.GetComponent<Player>().lessLive;
           //DIRECCIÓ ON SURT LA FIREBALL
            if (facingLeft)
                b.GetComponent<Rigidbody2D>().AddForce(new Vector2(-150f, 0));
            else
                b.GetComponent<Rigidbody2D>().AddForce(new Vector2(150f, 0));

            Physics2D.IgnoreCollision(b.GetComponent<CircleCollider2D>(), this.GetComponent<BoxCollider2D>());
            Physics2D.IgnoreCollision(b.GetComponent<CircleCollider2D>(), player.GetComponent<BoxCollider2D>());
            anim.SetTrigger("isThrowingBall");
            cooldownBall = true;
            Invoke("reactivateCooldown", 0.45f);
            soundmanager.GetComponent<SoundManager>().Play_SFXMarioFireBall();
        }

        //SHIELD
        if (Input.GetKeyDown("h"))
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXShieldOn();
            flagMove = false;
            flagShield = true;
            shield.SetActive(true);
            colliderBox.SetActive(false);
        }
        if (Input.GetKeyUp("h"))
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXShieldOff();
            flagMove = true;
            flagShield = false;
            shield.SetActive(false);
            colliderBox.SetActive(true);
        }

        //PARTICLES
        if (this.GetComponent<Transform>().position.x > 9.2)
        {
            if (!particles.GetComponent<ParticleSystem>().isPlaying)
            {
                soundmanager.GetComponent<SoundManager>().Play_SFXStageFall();
                particles.GetComponent<Transform>().position = new Vector2(particles.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                particles.GetComponent<ParticleSystem>().Play(true);
                Invoke("restartEnemy", 0.5f);
            }
           
        }
        else if (this.GetComponent<Transform>().position.x < -9.43)
        {
            if (!particles2.GetComponent<ParticleSystem>().isPlaying)
            {
                soundmanager.GetComponent<SoundManager>().Play_SFXStageFall();
                particles2.GetComponent<Transform>().position = new Vector2(particles2.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                particles2.GetComponent<ParticleSystem>().Play(true);
                Invoke("restartEnemy", 0.5f);
            }
        }
        else if (this.GetComponent<Transform>().position.y < -5.7)
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXStageFall();
            Invoke("restartEnemy", 0f);
        }

        SetAnimationState();
    }

    void LateUpdate()
    {
        CheckWhereToFace();
    }

    void SetAnimationState()
    {
        if (dirX == 0)
        {
            anim.SetBool("isRunning", false);

            if (comboStatus == 0)
            {
                anim.SetBool("isAttack1", false);
                anim.SetBool("isAttack2", false);
                anim.SetBool("isAttack3A", false);
                anim.SetBool("isAttack3B", false);
                if (!iframes && !flagShield)
                    flagMove = true;
            }
            else if (comboStatus == 1)
            {
                anim.SetBool("isAttack1", true);
            }
            else if (comboStatus == 2)
            {
                anim.SetBool("isAttack1", false);
                anim.SetBool("isAttack2", true);
            }
            else if (comboStatus == 3)
            {
                anim.SetBool("isAttack2", false);
                anim.SetBool("isAttack3A", true);
            }
            else if (comboStatus == 4)
            {
                anim.SetBool("isAttack2", false);
                anim.SetBool("isAttack3B", true);
            }


        }
        if ((dirX == velocity || dirX == -velocity) && rb.velocity.y == 0)
        {
            anim.SetBool("isRunning", true);
        }

        if (rb.velocity.y > 0)
        {
            anim.SetBool("isJumping", true);
        }
        else if (rb.velocity.y == 0)
        {
            anim.SetBool("isJumping", false);
        }
    }

    void CheckWhereToFace()
    {
        if (dirX > 0)
        {
            facingLeft = false;
        }
        else if (dirX < 0)
        {
            facingLeft = true;
        }

        if (((facingLeft) && (localScale.x < 0)) || ((!facingLeft) && (localScale.x > 0)))
        {
            localScale.x *= -1;
        }

        transform.localScale = localScale;
    }

    IEnumerator combo(string _key, float _wait)
    {
        if (_key == "o")
        {
            if (comboStatus == 0)
            {
                //PRIMER COMBO
                soundmanager.GetComponent<SoundManager>().Play_SFXMarioAttack1();

                comboStatus = 1;
                yield return new WaitForSeconds(_wait);
                if (comboStatus == 1)
                {
                    //COMBO FALLAT PER TEMPS
                    comboStatus = 0;
                }
            }
            else if (comboStatus == 1)
            {
                //SEGON COMBO
                soundmanager.GetComponent<SoundManager>().Play_SFXMarioAttack2();

                comboStatus = 2;
                yield return new WaitForSeconds(_wait);
                if (comboStatus == 2)
                {
                    //COMBO FALLAT PER TEMPS
                    comboStatus = 0;
                }
            }
            else if (comboStatus == 2)
            {
                //TERCER COMBO A
                soundmanager.GetComponent<SoundManager>().Play_SFXMarioAttack3();

                comboStatus = 3;
                yield return new WaitForSeconds(_wait);
                if (comboStatus == 3)
                {
                    //COMBO FALLAT PER TEMPS
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                //COMBO FALLAT
            }
        }
        else if (_key == "p")
        {
            if (comboStatus == 2)
            {
                //TERCER COMBO B
                soundmanager.GetComponent<SoundManager>().Play_SFXMarioAttack4();
                comboStatus = 4;
                yield return new WaitForSeconds(_wait);
                if (comboStatus == 4)
                {
                    //COMBO FALLAT PER TEMPS
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                //COMBO FALLAT
            }
        }
        else
        {
            comboStatus = 0;
            //TECLA INCORRECTA
        }

    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Plataforma":
                if (this.GetComponent<Transform>().position.y > collision.gameObject.GetComponent<Transform>().position.y)
                {
                    //BAIXA DE LA PLATAFORMA
                    if (Input.GetKey("k"))
                    {
                        this.GetComponent<BoxCollider2D>().enabled = false;
                    }
                }
                break;
            case "Plataforma2":
                if (!isGrounded)
                {
                    isGrounded = true;
                    doubleJump = false;
                }
                break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            //AIXÍ POT SALTAR EN L'AIGUA
            case "Water":
                isGrounded = true;
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Water":
                soundmanager.GetComponent<SoundManager>().Play_SFXWater();
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Ground":
                if(!isGrounded && rb.velocity.y == 0)
                {
                    isGrounded = true;
                    doubleJump = false;
                }
                break;
            case "Plataforma":
                if (!isGrounded && rb.velocity.y == 0)
                {
                    isGrounded = true;
                    doubleJump = false;
                }
                break;
        }
    }

    //ACTIVA COLLIDER AL BAIXAR DE LA PLATAFORMA
    void activateCollider()
    {
        this.GetComponent<BoxCollider2D>().enabled = true;
    }

    //TORNA EL PERSONATGE A L'ESCENARI QUAN EL FAN FORA
    void restartEnemy()
    {
        this.GetComponent<Transform>().position = new Vector2(1.36f, 1);
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        this.vida = 1f;
        stock--;
        if (updateEnemyUIEvent != null)
        {
            updateEnemyUIEvent.Invoke(stock);
        }
    }

    //TREUEN VIDA AL PERSONATGE
    public void lessLive(float n)
    {
        this.vida += n;
        soundmanager.GetComponent<SoundManager>().Play_SFXMarioDamage();
        if (!iframes)
        {
            flagMove = false;
            iframes = true;
            Invoke("reactivateIframes", 1f);
           
        }
        if(updateEnemyUIEvent != null)
        {
            updateEnemyUIEvent.Invoke(stock);
        }
    }

    void reactivateIframes()
    {
        iframes = false;
        flagMove = true;
    }

    void reactivateCooldown()
    {
        cooldownBall = false;
    }

    public int getComboStatus()
    {
        return comboStatus;
    }
}
