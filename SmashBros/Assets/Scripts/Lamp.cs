﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour
{
    public GameObject m_light;
    GameObject soundmanager;

    void Start()
    {
        //CADA 30 SEGONS ES TORNA A OBRIR LA LLUM
        InvokeRepeating("restartIntensity", 0f, 30f);
        soundmanager = GameObject.Find("SoundManager");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            //SI ALGUN ATAC TOCA LA LLUM, AQUESTA ANIRÀ BAIXANT D'INTENSITAT
            case "Hitbox":
                if(m_light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity > 0)
                    m_light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity -= 0.2f;
                soundmanager.GetComponent<SoundManager>().Play_SFXLamp();
                break;
        }
    }

    void restartIntensity()
    {
        m_light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 1f;
    }


}
