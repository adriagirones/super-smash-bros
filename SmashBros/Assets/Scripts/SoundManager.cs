﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    //MARIO SFX
    public AudioSource sfx_mariofireball;
    public AudioSource sfx_mariojump;
    public AudioSource sfx_mariojump2;
    public AudioSource sfx_marioattack1;
    public AudioSource sfx_marioattack2;
    public AudioSource sfx_marioattack3;
    public AudioSource sfx_marioattack4;
    public AudioSource sfx_mariodamage1;
    public AudioSource sfx_mariodamage2;
    public AudioSource sfx_mariodamage3;

    //LINK SFX
    public AudioSource sfx_linkattack1;
    public AudioSource sfx_linkattack2;
    public AudioSource sfx_linkattack3;
    public AudioSource sfx_linkattack4;
    public AudioSource sfx_linkjump;
    public AudioSource sfx_linkdamage1;
    public AudioSource sfx_linkdamage2;
    public AudioSource sfx_linkdamage3;
    public AudioSource sfx_linkdamage4;
    public AudioSource sfx_linkthrowbomb;
    public AudioSource sfx_bomb;

    //GENERAL SFX
    public AudioSource sfx_shieldon;
    public AudioSource sfx_shieldoff;
    public AudioSource sfx_lamp;
    public AudioSource sfx_water;
    public AudioSource sfx_stagefall;
    public AudioSource sfx_destroyplatform;

    public AudioSource bgm_music;

    void Start()
    {
        bgm_music.Play();
    }

    public void Play_SFXMarioFireBall()
    {
        sfx_mariofireball.Play();
    }

    public void Play_SFXMarioJump()
    {
        sfx_mariojump.Play();
    }

    public void Play_SFXMarioJump2()
    {
        sfx_mariojump2.Play();
    }

    public void Play_SFXMarioAttack1()
    {
        sfx_marioattack1.Play();
    }

    public void Play_SFXMarioAttack2()
    {
        sfx_marioattack2.Play();
    }

    public void Play_SFXMarioAttack3()
    {
        sfx_marioattack3.Play();
    }

    public void Play_SFXMarioAttack4()
    {
        sfx_marioattack4.Play();
    }

    public void Play_SFXMarioDamage()
    {
        int r = Random.Range(0, 3);
        if (r == 0 && (!sfx_mariodamage1.isPlaying && !sfx_mariodamage2.isPlaying && !sfx_mariodamage3.isPlaying))
            sfx_mariodamage1.Play();
        else if (r == 1 && (!sfx_mariodamage1.isPlaying && !sfx_mariodamage2.isPlaying && !sfx_mariodamage3.isPlaying))
            sfx_mariodamage2.Play();
        else if (!sfx_mariodamage1.isPlaying && !sfx_mariodamage2.isPlaying && !sfx_mariodamage3.isPlaying)
            sfx_mariodamage3.Play();
    }

    public void Play_SFXLinkJump()
    {
        sfx_linkjump.Play();
    }

    public void Play_SFXLinkAttack1()
    {
        sfx_linkattack1.Play();
    }

    public void Play_SFXLinkAttack2()
    {
        sfx_linkattack2.Play();
    }

    public void Play_SFXLinkAttack3()
    {
        sfx_linkattack3.Play();
    }

    public void Play_SFXLinkAttack4()
    {
        sfx_linkattack4.Play();
    }

    public void Play_SFXLinkDamage()
    {
        int r = Random.Range(0, 3);
        if (r == 0 && (!sfx_linkdamage1.isPlaying && !sfx_linkdamage2.isPlaying && !sfx_linkdamage3.isPlaying && !sfx_linkdamage4.isPlaying))
            sfx_linkdamage1.Play();
        else if (r == 1 && (!sfx_linkdamage1.isPlaying && !sfx_linkdamage2.isPlaying && !sfx_linkdamage3.isPlaying && !sfx_linkdamage4.isPlaying))
            sfx_linkdamage2.Play();
        else if (r == 2 && (!sfx_linkdamage1.isPlaying && !sfx_linkdamage2.isPlaying && !sfx_linkdamage3.isPlaying && !sfx_linkdamage4.isPlaying))
            sfx_linkdamage3.Play();
        else if (!sfx_linkdamage1.isPlaying && !sfx_linkdamage2.isPlaying && !sfx_linkdamage3.isPlaying && !sfx_linkdamage4.isPlaying)
            sfx_linkdamage4.Play();
    }

    public void Play_SFXLinkThrowBomb()
    {
        sfx_linkthrowbomb.Play();
    }

    public void Play_SFXBomb()
    {
        sfx_bomb.Play();
    }

    public void Play_SFXShieldOn()
    {
        sfx_shieldon.Play();
    }

    public void Play_SFXShieldOff()
    {
        sfx_shieldoff.Play();
    }

    public void Play_SFXLamp()
    {
        sfx_lamp.Play();
    }

    public void Play_SFXWater()
    {
        sfx_water.Play();
    }

    public void Play_SFXStageFall()
    {
        sfx_stagefall.Play();
    }

    public void Play_SFXDestroyPlatform()
    {
        sfx_destroyplatform.Play();
    }

}
