﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocity;
    public float vida = 1f;

    GameObject hitbox1;
    GameObject hitbox2;
    GameObject colliderBox;
    GameObject shield;

    public GameObject bomb;
    public GameObject enemy;

    public GameObject particles;
    public GameObject particles2;

    Animator anim;
    Rigidbody2D rb;

    int comboStatus;
    bool isGrounded;
    bool doubleJump;

    float dirX = 0f;

    bool facingRight = true;
    Vector3 localScale;

    bool flagMove = true;
    bool flagShield = false;
    bool iframes = false;
    bool cooldownBomb = false;

    public delegate void updatePlayerUI(int n);
    public event updatePlayerUI updatePlayerUIEvent;

    public Pool poolManager;

    int stock;

    GameObject soundmanager;

    void Start()
    {
        colliderBox = transform.GetChild(1).gameObject;
        shield = transform.GetChild(2).gameObject;
        hitbox1 = transform.GetChild(3).gameObject;
        hitbox2 = transform.GetChild(4).gameObject;
        
        colliderBox.GetComponent<PlayerTrigger>().activateColliderEvent += activateCollider;
        hitbox1.SetActive(false);
        hitbox2.SetActive(false);
        shield.SetActive(false);
        
        anim = GetComponent<Animator>();
        rb = this.GetComponent<Rigidbody2D>();
        localScale = transform.localScale;

        GameObject enemyHitbox1 = enemy.transform.GetChild(3).gameObject;
        GameObject enemyHitbox2 = enemy.transform.GetChild(4).gameObject;
        enemyHitbox1.GetComponent<EnemyHitbox>().damageEnemyEvent += lessLive;
        enemyHitbox2.GetComponent<EnemyHitbox>().damageEnemyEvent += lessLive;

        stock = 3;

        soundmanager = GameObject.Find("SoundManager");
    }

    void Update()
    {
        //MOVIMENT
        if (Input.GetKey("a") && flagMove)
        {
            dirX = -velocity;
            rb.velocity = new Vector2(-velocity, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey("d") && flagMove)
        {
            dirX = velocity;
            rb.velocity = new Vector2(velocity, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            dirX = 0;
        }
        if (!isGrounded)
        {
            rb.velocity = new Vector2(dirX, rb.velocity.y);
        }

        //DOBLE SALT
        if (!isGrounded && Input.GetKeyDown("w") && !doubleJump && flagMove)
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXLinkJump();
            //RESETEJA LA VELOCITAT PER FER UN DOBLE SALT AMB LA MATEIXA FORÇA QUE EL PRIMER
            rb.velocity = new Vector2(0, 0);
            rb.AddForce(new Vector2(0, 350f));
            doubleJump = true;
        }
        //SALT
        if (isGrounded && Input.GetKeyDown("w") && flagMove)
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXLinkJump();
            rb.AddForce(new Vector2(0, 350f));
            isGrounded = false;
        }

        //COMBOS
        if (Input.GetKeyDown("e") && !flagShield)
        {
            flagMove = false;
            if (isGrounded)
                StartCoroutine(combo("e", 0.5f));
        }
        else if (Input.GetKeyDown("r") && !flagShield)
        {
            if (isGrounded)
                StartCoroutine(combo("r", 0.8f));
        }

        //BOMB
        if (Input.GetKeyDown("q") && flagMove && !cooldownBomb)
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXLinkThrowBomb();
            Invoke("throwNewBomb", 0.5f);
            anim.SetTrigger("isThrowingBomb");
            cooldownBomb = true;
            Invoke("reactivateCooldown", 1f);
        }

        //SHIELD
        if (Input.GetKeyDown("f")) {
            soundmanager.GetComponent<SoundManager>().Play_SFXShieldOn();
            flagMove = false;
            flagShield = true;
            shield.SetActive(true);
            colliderBox.SetActive(false);
        }
        if (Input.GetKeyUp("f"))
        {
            soundmanager.GetComponent<SoundManager>().Play_SFXShieldOff();
            flagMove = true;
            flagShield = false;
            shield.SetActive(false);
            colliderBox.SetActive(true);
        }


        //PARTICLES
        if (this.GetComponent<Transform>().position.x > 9.2)
        {
            if (!particles.GetComponent<ParticleSystem>().isPlaying)
            {
                soundmanager.GetComponent<SoundManager>().Play_SFXStageFall();
                particles.GetComponent<Transform>().position = new Vector2(particles.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                particles.GetComponent<ParticleSystem>().Play(true);
                Invoke("restartPlayer", 0.5f);
            }

        }
        else if (this.GetComponent<Transform>().position.x < -9.43)
        {
            if (!particles2.GetComponent<ParticleSystem>().isPlaying)
            {
                soundmanager.GetComponent<SoundManager>().Play_SFXStageFall();
                particles2.GetComponent<Transform>().position = new Vector2(particles2.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                particles2.GetComponent<ParticleSystem>().Play(true);
                Invoke("restartPlayer", 0.5f);
            }
            flagMove = false;
        }
        else if (this.GetComponent<Transform>().position.y < -5.7) {
            soundmanager.GetComponent<SoundManager>().Play_SFXStageFall();
            Invoke("restartPlayer", 0f);
        }

        SetAnimationState();
    }

    void LateUpdate()
    {
        CheckWhereToFace();
    }

    void SetAnimationState()
    {
        if(dirX == 0)
        {
            anim.SetBool("isRunning", false);

            if (comboStatus == 0)
            {
                anim.SetBool("isAttack1", false);
                anim.SetBool("isAttack2", false);
                anim.SetBool("isAttack3A", false);
                anim.SetBool("isAttack3B", false);
                if(!iframes && !flagShield)
                    flagMove = true;
            }
            else if (comboStatus == 1)
            {
                anim.SetBool("isAttack1", true);
            }
            else if (comboStatus == 2)
            {
                anim.SetBool("isAttack1", false);
                anim.SetBool("isAttack2", true);
            }
            else if (comboStatus == 3)
            {
                anim.SetBool("isAttack2", false);
                anim.SetBool("isAttack3A", true);
            }
            else if (comboStatus == 4)
            {
                anim.SetBool("isAttack2", false);
                anim.SetBool("isAttack3B", true);
            }
        }
        if ((dirX == velocity || dirX == -velocity) && rb.velocity.y == 0)
        {
            anim.SetBool("isRunning", true);
        }

        if(rb.velocity.y > 0)
        {
            anim.SetBool("isJumping", true);
        }
        else if (rb.velocity.y == 0)
        {
            anim.SetBool("isJumping", false);
        }

    }

    void CheckWhereToFace()
    {
        if(dirX > 0)
        {
            facingRight = true;
        }
        else if(dirX < 0)
        {
            facingRight = false;
        }

        if(((facingRight) && (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0)))
        {
            localScale.x *= -1;
        }

        transform.localScale = localScale;
    }


    IEnumerator combo(string _key, float _wait)
    {
        if (_key == "e")
        {
            if (comboStatus == 0)
            {
                //PRIMER COMBO
                comboStatus = 1;
                soundmanager.GetComponent<SoundManager>().Play_SFXLinkAttack1();
                yield return new WaitForSeconds(_wait);
                if (comboStatus == 1)
                {
                    //COMBO FALLAT PER TEMPS
                    comboStatus = 0;
                }
            }
            else if (comboStatus == 1)
            {
                //SEGON COMBO
                comboStatus = 2;
                soundmanager.GetComponent<SoundManager>().Play_SFXLinkAttack2();
                yield return new WaitForSeconds(_wait);
                if (comboStatus == 2)
                {
                    //COMBO FALLAT PER TEMPS
                    comboStatus = 0;
                }
            }
            else if (comboStatus == 2)
            {
                //TERCER COMBO A
                comboStatus = 3;
                soundmanager.GetComponent<SoundManager>().Play_SFXLinkAttack3();
                yield return new WaitForSeconds(_wait);
                if (comboStatus == 3)
                {
                    //COMBO FALLAT PER TEMPS
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                //COMBO FALLAT
            }
        }
        else if (_key == "r")
        {
            if (comboStatus == 2)
            {
                //TERCER COMBO B
                comboStatus = 4;
                soundmanager.GetComponent<SoundManager>().Play_SFXLinkAttack4();
                yield return new WaitForSeconds(_wait);
                if (comboStatus == 4)
                {
                    //COMBO FALLAT PER TEMPS
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                //COMBO FALLAT
            }
        }
        else
        {
            comboStatus = 0;
            //TECLA INCORRECTA
        }

    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Plataforma":
                if (this.GetComponent<Transform>().position.y > collision.gameObject.GetComponent<Transform>().position.y)
                {
                    //BAIXA DE LA PLATAFORMA
                    if (Input.GetKey("s"))
                    {
                        this.GetComponent<BoxCollider2D>().enabled = false;
                    }
                }
                break;
            case "Plataforma2":
                if (!isGrounded)
                {
                    isGrounded = true;
                    doubleJump = false;
                }
                break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            //AIXÍ POT SALTAR EN L'AIGUA
            case "Water":
                isGrounded = true;
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Water":
                soundmanager.GetComponent<SoundManager>().Play_SFXWater();
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Ground":
                if (!isGrounded && rb.velocity.y == 0)
                {
                    isGrounded = true;
                    doubleJump = false;
                }
                break;
            case "Plataforma":
                if(!isGrounded && rb.velocity.y == 0)
                {
                    isGrounded = true;
                    doubleJump = false;
                }
                break;
        }
    }

    //ACTIVA COLLIDER AL BAIXAR DE LA PLATAFORMA
    void activateCollider()
    {
        this.GetComponent<BoxCollider2D>().enabled = true;
    }

    //TORNA EL PERSONATGE A L'ESCENARI QUAN EL FAN FORA
    void restartPlayer()
    {
        this.GetComponent<Transform>().position = new Vector2(-2.5f, 1);
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        this.vida = 1f;
        stock--;
        if (updatePlayerUIEvent != null)
        {
            updatePlayerUIEvent.Invoke(stock);
        }
    }

    //TREUEN VIDA AL PERSONATGE
    public void lessLive(float n)
    {
        this.vida += n;
        soundmanager.GetComponent<SoundManager>().Play_SFXLinkDamage();
        if (!iframes)
        {
            flagMove = false;
            iframes = true;
            Invoke("reactivateIframes", 0.5f);
        }
        if (updatePlayerUIEvent != null)
        {
            updatePlayerUIEvent.Invoke(stock);
        }
    }

    void throwNewBomb()
    {
        GameObject b = poolManager.GetObjFromPool();
        b.GetComponent<Bomb>().enableBomb();
        b.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y + 1.2f);

        for (int i = 0; i < b.transform.childCount; ++i)
        {
            Transform currentChild = b.transform.GetChild(i);
            if (currentChild.name.Equals("Hitbox"))
            {
                currentChild.gameObject.GetComponent<BombHitbox>().damagePlayerEvent += enemy.GetComponent<Enemy>().lessLive;
            }
        }

        if (facingRight)
            b.GetComponent<Rigidbody2D>().AddForce(new Vector2(350f, 350f));
        else
            b.GetComponent<Rigidbody2D>().AddForce(new Vector2(-350f, 350f));
        Physics2D.IgnoreCollision(b.GetComponent<CircleCollider2D>(), this.GetComponent<BoxCollider2D>());
    }

    void reactivateIframes()
    {
        iframes = false;
        flagMove = true;
    }

    void reactivateCooldown()
    {
        cooldownBomb = false;
    }

    public int getComboStatus()
    {
        return comboStatus;
    }


}
